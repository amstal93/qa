#!/usr/bin/env bash
set -Eeuo pipefail
    
IMAGES=(cpp-qa php-qa python-qa)    
REGISTRY_HOST=registry.gitlab.com
USER=stelgenhof
REPOSITORY=qa

RED='\033[0;31m'
GREEN='\033[0;32m'
BRIGHT_GREEN='\033[1;32m'
YELLOW='\033[1;33m'
MAGENTA='\033[1;35m'
NC='\033[0m' # No Color


echo -e "${GREEN}Building images...${NC}"
docker login $REGISTRY_HOST

for ((i = 0; i < ${#IMAGES[@]}; ++i)); do
    image=${IMAGES[$i]}

    echo -e "${MAGENTA} - $image image${NC}"

    docker build -t $REGISTRY_HOST/$USER/$REPOSITORY/$image -f $image/Dockerfile .
    docker push $REGISTRY_HOST/$USER/$REPOSITORY/$image
done

echo
echo -e "${BRIGHT_GREEN}Finished!!! \o/${NC}"
